﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using Test_Inactivos;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test15_Crear_Usuario_Cancelar_Administradorr
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
      
        Lectura_Test_Inactivos lecEstado = new Lectura_Test_Inactivos();
        static login login = new login();
        CancelAdministrador canc = new CancelAdministrador();
        /*
        * CP01-15	Crear usuario (Administrador) Cancelar
        */
          [Test]
#pragma warning disable CS1591
        public void Test15_Crear_Usuario_Cancelar_Administrador_()
        {
            if (!lecEstado.LecturaTestInactivos("Crear_Usuario_Cancelar_Admin"))
            {
             driver = Conf.ConfigChrome();
            login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
            canc.altaUsuarioBOClaroCrearUsuarioBtnCancelar(driver, lista, LocalizadoresBO._011101_linkAdministradores);
            }
            else
            {
                Assert.Ignore("Test ignored during Prod runs");
            }
        }

    }
}