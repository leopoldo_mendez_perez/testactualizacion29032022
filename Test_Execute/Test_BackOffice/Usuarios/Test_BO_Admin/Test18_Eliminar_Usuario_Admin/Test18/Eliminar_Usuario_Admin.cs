﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Test_Execute.Selenium.ResxBO;
using Test_Execute.Selenium.ResxConfGlob;
using OpenQA.Selenium.Support.UI;
using System.Reflection;
using System.Configuration;
using AventStack.ExtentReports;
using NUnit.Framework;

namespace Test_Execute.Selenium.suit
{
    class Eliminación_Usuario_Administrador
    {

        Selenium Functions = new Selenium();
        static String[] part; // variable para almacenar las cadenas desintegradas
        /*
          * Fecha: 23.02.22
          * Creador: Leopoldo Mendez Perez (QA)
          * Objetivo: Método para la eliminación de usuario Administrador 
          * Ultima actualización:
          * Responsable Ultima actualización:
          * Objetivo ultima actualización:
          */
        public void EliminacionUsuarioAdminBOClaro(ChromeDriver driver, List<String> lista)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 10);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011601_opcionAdministrador, lista, "0", 10);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010801_inputSearch, $"{DatosBO._011101_usuarioAdministrador}", lista, "0");
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010901_buttonUserEdit, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011801_EliminarUsuarioAdmin, lista, "0", 0);
            Thread.Sleep(1000);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011802_confirmEliminarUsuarioAdmin, lista, "0", 10);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ExistWhitParameter(driver, LocalizadoresBO._011803_textUsuarioAdminElim, 3);
            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            driver.Quit();
        }
    }
}
