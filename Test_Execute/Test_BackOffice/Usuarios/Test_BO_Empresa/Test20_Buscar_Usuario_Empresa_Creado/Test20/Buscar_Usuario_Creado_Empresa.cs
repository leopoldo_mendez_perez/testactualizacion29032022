﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Threading;
using Test_Execute.Selenium.ResxBO;


namespace Test_Execute.Selenium.suit
{
    class Busqueda_Usaurio_Empresa
    {

        Selenium Functions = new Selenium();
       
        /*
     * Fecha: 30.12.21
     * Creador: Leopoldo Mendez Perez (QA)
     * Objetivo: Método para la busqueda de usuario Multipaís
     * Ultima actualización 23-02-2022:
     * Responsable Ultima actualización:
     * Objetivo ultima actualización:
     */
        public void BusquedaUsuarioEmpMultEmpMultPaisBOClaroreado(ChromeDriver driver, List<String> lista, String usuarioAdmin, String xpath, String siglas)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 10);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", xpath, lista, "0", 0);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010801_inputSearch, siglas + $"{usuarioAdmin}" + DatosBO._012002_SigP2, lista, "0");
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010802_columFecha, lista, "0", 0);
            Functions.ExistWhitParameter(driver, LocalizadoresBO._010803_validUserP1 + siglas + usuarioAdmin + DatosBO._012002_SigP2 + LocalizadoresBO._010803_validUserP2, 3);
            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            driver.Quit();  
       }




    }
}
