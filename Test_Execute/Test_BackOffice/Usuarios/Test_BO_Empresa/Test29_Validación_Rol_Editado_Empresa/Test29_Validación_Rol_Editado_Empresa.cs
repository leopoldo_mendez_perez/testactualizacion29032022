﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;
using Test_Inactivos;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test29_Validación_Rol_Editado_Empresa
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        login login = new login();
        private static List<String> lista = new List<String>();
      
        Lectura_Test_Inactivos lecEstado = new Lectura_Test_Inactivos();
        ValidacionEdicionUsuario_Empresa t = new ValidacionEdicionUsuario_Empresa();
        /*
              * CP01-10	Validacion Editar Usuario (SuperAdministrador) Edicion de Rol
              */
        [Test]
        public void Test29_Validación_Rol_Editado_Empresa_()
        {
            if (!lecEstado.LecturaTestInactivos("Validación_Edicion_Rol_Usuario_Empresa"))
            {
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                t.ValidacionEdicionUsuarioBOClaroread(driver, lista, LocalizadoresBO._013201_opciónBusqEmpresa, LocalizadoresBO._013903_textVadi, LocalizadoresBO._013201_opciónBusqEmpresa);
            }
            else
            {
                Assert.Ignore("Test ignored during Prod runs");
            }
        }

    }
}