﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test08_Busqueda_Usuario_Creado_SupAdmin
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        static BusquedaUsuarioCreado busquedaUsuario = new BusquedaUsuarioCreado();
        static login login = new login();
        private static List<String> lista = new List<String>();
      
        /////////////////////////////////////////////////////////////////////////
        /*
        * CP01-08	buscar usuario (SuperAdministrador) busqeuda de usuario Creado
        */
        [Test]
        public void Test08_Busqueda_Usuario_Creado_SupAdmin_()
        {
            
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                busquedaUsuario.BusquedaUsuarioBOClaroreado(driver, lista, DatosBO._010401_nombre_Usuario, LocalizadoresBO._010908_opcionSuperAdministrador, DatosBO._010802_SupAdmSigP1);

           
        }

    }
}