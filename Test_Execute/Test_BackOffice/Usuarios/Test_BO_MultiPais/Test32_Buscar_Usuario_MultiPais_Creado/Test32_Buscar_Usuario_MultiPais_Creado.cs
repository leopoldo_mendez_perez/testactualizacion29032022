﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test32_Buscar_Usuario_MultiPais_Creado
    {
      
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
        Busqueda_Usaurio_MultiPais bus = new Busqueda_Usaurio_MultiPais();
        static login login = new login();
        /*
        * CP01-32 buscar usuario (MultiPais) busqueda de usuario Creado
        */
        [Test]
        public void Test32_Buscar_Usuario_MultiPais_Creado_()
        {
      
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                bus.BusquedaUsuarioEmpMultEmpMultPaisBOClaroreado(driver, lista, DatosBO._013102_usuarioAdmin, LocalizadoresBO._013201_opciónBusqEmpresa, DatosBO._013805_adminSigP1);
           
        }

    }
}