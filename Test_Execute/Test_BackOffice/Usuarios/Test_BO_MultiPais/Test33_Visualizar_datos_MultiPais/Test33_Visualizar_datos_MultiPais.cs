﻿using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test33_Visualizar_datos_MultiPais
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
        static login login = new login();
        Visualización_Datos_MultiPais vis = new Visualización_Datos_MultiPais();
        /*
        * CP01-33	buscar usuario (MultiPais) busqueda de usuario Creado
        */
        [Test]
        public void Test33_Visualizar_datos_MultiPais_()
        {
           
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                vis.VerDatosUsuarioEmpresaBOClaroreado(driver, lista, DatosBO._013102_usuarioAdmin, LocalizadoresBO._013201_opciónBusqEmpresa, DatosBO._012001_SigP1);
           
        }

    }
}