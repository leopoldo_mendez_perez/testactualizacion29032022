﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test40_Edición_Rol_Usuario_MultiPais
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
        Edicion_Rol_Usuario_MultiPais edi = new Edicion_Rol_Usuario_MultiPais();
        static login login = new login();

        /*
        * CP01-40 Usuario (MultiPais) Edicion de Rol
        */
        [Test]
        public void Test40_Edición_Rol_Usuario_MultiPais_()
        {
        
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                 edi.EdicionRolUsuarioEmpresa(driver, lista,  LocalizadoresBO._013501_textoEvaluar, LocalizadoresBO._013201_opciónBusqEmpresa);
           
        }
    }
}