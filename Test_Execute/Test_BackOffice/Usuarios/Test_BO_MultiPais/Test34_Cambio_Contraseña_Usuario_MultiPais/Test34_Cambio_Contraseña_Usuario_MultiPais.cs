﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test34_Cambio_Contraseña_Usuario_MultiEmpresa
    {

        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
      
        static login login = new login();
        Cambio_Contraseña_Usuario_MultiPais c = new Cambio_Contraseña_Usuario_MultiPais();
        /*
		* CP01-22Usuario (Empresa) Cambio de contraseña
		*/
        [Test]
        public void Test34_Cambio_Contraseña_Usuario_MultiEmpresa_()
        {
           

                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
               c.CambioContraseñaUsuarioEmpresaBOClaroreado(driver, lista, LocalizadoresBO._013201_opciónBusqEmpresa, LocalizadoresBO._012205_textConfirm,DatosBO._012001_SigP1, DatosBO._013102_usuarioAdmin);
           
        }

    }
}