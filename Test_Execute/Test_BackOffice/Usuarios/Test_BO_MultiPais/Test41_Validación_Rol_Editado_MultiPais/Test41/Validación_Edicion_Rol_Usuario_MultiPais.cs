﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using Test_Execute.Selenium.ResxBO;

namespace Test_Execute.Selenium.suit
{
    class ValidacionEdicionUsuario_MultiPais
    {

        Selenium Functions = new Selenium();
        string ruta = @"C:\Logs_TE_TER\";
       
        string Orden;
        static String[] part; // variable para almacenar las cadenas desintegradas
        /*
        * Fecha:21.02.22
        * Creador: Leopoldo Méndez Pérez (QA)
        * Objetivo: Método para la validación de los roles editados
        * Ultima actualización:14-03-2022
        * Responsable Ultima actualización:Leopoldo Méndez Pérez (QA Sigel)
        * Objetivo ultima actualización:Incorporación nueva Mejora para la ejecución de test
        */
        public void ValidacionEdicionUsuarioBOClaroread (ChromeDriver driver, List<string> lista,String xpath)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 10);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", xpath, lista, "0", 10);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010801_inputSearch, DatosBO._012001_SigP1 + $"{ DatosBO._013801_usuarioNuevo}" + DatosBO._012002_SigP2, lista, "0");
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010802_columFecha, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010901_buttonUserEdit, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._012801_Editar_Perfil, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            part = Functions.SeparaTexto(DatosBO._014001_rolUsuario_MultiPais);
            String numerofin, textofin;
            for (int i = 0; i < part.Length; i++)
            {
                //Lineas de codigo para extraer el estado del rol y tipo de Rol;

                textofin = (part[i].Substring(2)).Replace(" ", "");
                if (i == 0)
                {
                    numerofin = (part[i].Substring(0, 1)).Replace(" ", "");
                }
                else
                {
                    numerofin = (part[i].Substring(1, 1)).Replace(" ", "");
                }
                //---------------------------------------
                //ROL Consultas
                if (numerofin == "1" && textofin == "Consultas")
                {
                    Functions.ExistWhitParameter(driver,LocalizadoresBO._0128014_Rol_EditadoP1+ "Consultas" + LocalizadoresBO._0128015_Rol_EditadoP2, 3);
                    Functions.ExistWhitParameter(driver, LocalizadoresBO._012803_ValConsLlamada, 3);
                    Functions.ExistWhitParameter(driver, LocalizadoresBO._012804_ValConsultasConsumo, 3);
                    Functions.ExistWhitParameter(driver, LocalizadoresBO._012805_ValConsultasFinanciamiento, 3);
                    Functions.ExistWhitParameter(driver, LocalizadoresBO._012806_ValConsultasServicioContr, 3);
                    Console.WriteLine("Validación de Rol Editado: "+textofin);
                }
                //ROL Consultas de detalles de llamadas
                if (numerofin == "1" && textofin == "Consultasdedetallesdellamadas")
                {

                    Functions.ExistWhitParameter(driver, LocalizadoresBO._012803_ValConsLlamada, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);
                }
                //ROL Consultas de consumos de paquetes
                if (numerofin == "1" && textofin == "Consultasdeconsumosdepaquetes")
                {

                    Functions.ExistWhitParameter(driver, LocalizadoresBO._012804_ValConsultasConsumo, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);
                }
                //ROL Consultas de financiamientos
                if (numerofin == "1" && textofin == "Consultasdefinanciamientos")
                {

                    Functions.ExistWhitParameter(driver, LocalizadoresBO._012805_ValConsultasFinanciamiento, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);
                }
                //ROL Consultas de servicios contratados
                if (numerofin == "1" && textofin == "Consultasdeservicioscontratados")
                {

                    Functions.ExistWhitParameter(driver, LocalizadoresBO._012806_ValConsultasServicioContr, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);
                }

                //ROL Facturación
                if (numerofin == "1" && textofin == "Facturación")
                {
                    Functions.ExistWhitParameter(driver, LocalizadoresBO._0128014_Rol_EditadoP1 + "Facturación" + LocalizadoresBO._0128015_Rol_EditadoP2, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);

                }
                //ROL Paquetes
                if (numerofin == "1" && textofin == "Paquetes")
                {
                    Functions.ExistWhitParameter(driver, LocalizadoresBO._0128014_Rol_EditadoP1 + "Paquetes" + LocalizadoresBO._0128015_Rol_EditadoP2, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);

                }
                //ROL Ayuda
                if (numerofin == "1" && textofin == "Ayuda")
                {
                    Functions.ExistWhitParameter(driver, LocalizadoresBO._0128014_Rol_EditadoP1 + "Ayuda" + LocalizadoresBO._0128015_Rol_EditadoP2, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);

                }
                //ROL Administración
                if (numerofin == "1" && textofin == "Administración")
                {
                    Functions.ExistWhitParameter(driver, LocalizadoresBO._0128014_Rol_EditadoP1 + "Administración" + LocalizadoresBO._0128015_Rol_EditadoP2, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);
                    Functions.ExistWhitParameter(driver, LocalizadoresBO._0128011_ValUsuarios, 3);
                    Functions.ExistWhitParameter(driver, LocalizadoresBO._0128012_ValCamposPersonalizados, 3);
                    Functions.ExistWhitParameter(driver, LocalizadoresBO._0128013_ValHistoraMens, 3);
                }
                //ROL Usuarios
                if (numerofin == "1" && textofin == "Usuarios")
                {
                    Functions.ExistWhitParameter(driver, LocalizadoresBO._0128011_ValUsuarios, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);

                }

                //ROL Campos personalizados
                if (numerofin == "1" && textofin == "Campospersonalizados")
                {
                    Functions.ExistWhitParameter(driver, LocalizadoresBO._0128012_ValCamposPersonalizados, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);

                }
                //Rol Historial demensajes
                if (numerofin == "1" && textofin == "Historialdemensajes")
                {
                    Functions.ExistWhitParameter(driver, LocalizadoresBO._0128013_ValHistoraMens, 3);
                    Console.WriteLine("Validación de Rol Editado: " + textofin);

                }
            }

            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            driver.Quit();
        }
    }
}
