﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test41_Validación_Rol_Editado_MultiPais
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        login login = new login();
        private static List<String> lista = new List<String>();
        ValidacionEdicionUsuario_MultiPais t = new ValidacionEdicionUsuario_MultiPais();
        /*
              * CP01-41	(MultiPais) Validacion Edicion de Rol
              */
        [Test]
        public void Test41_Validación_Rol_Editado_MultiPais_()
        {
            
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                t.ValidacionEdicionUsuarioBOClaroread(driver, lista, LocalizadoresBO._013201_opciónBusqEmpresa);
          
        }

    }
}