﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Threading;
using Test_Execute.Selenium.ResxBO;


namespace Test_Execute.Selenium.suit
{
    class Crear_Usuario_MultiPais
    {

        Selenium Functions = new Selenium();
       
        /*
         * Fecha: 30.12.21
         * Creador: Leopoldo Mendez Perez (QA)
         * Objetivo: Alta de usuario MultiPaís 
         * Ultima actualización:14.03.22
         * Responsable Ultima actualización:Leopoldo Mendez Perez (QA)
         * Objetivo ultima actualización: Incorporar nueva Mejora para la ejecucion de test.
         */
        public void altaUsuarioMultipaisBOClaro(ChromeDriver driver, List<String> lista)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 10);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.scrooll(driver, LocalizadoresBO._010404_buttonCrearUsuario);
            Functions.ClickButton(driver, "Id", LocalizadoresBO._010402_buttonList, lista, "0", 5);

            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011901_linkEmpresa, lista, "0", 5);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011914_tipo, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._013101_multiPais, lista, "0", 0);

            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._011902_nombrEmpresa, $"{DatosBO._013101_nombre}", lista, "0");
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._011908_usuarioAdmin, $"{DatosBO._013102_usuarioAdmin}", lista, "0");

            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._011903_correoAdminEmpresa, $"{DatosBO._013103_correoElectronico}", lista, "0");
            Functions.scrooll(driver, LocalizadoresBO._011904_telAdminEmpresa);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._011904_telAdminEmpresa, $"{DatosBO._013104_teléfono}", lista, "0");

            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011905_asignEjecPrev, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011916_selectEjecPrev, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011906_asignEjecPos, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011917_selectEjecPos, lista, "0", 0);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._011918_contasenia, $"{DatosBO._013105_contraseña}", lista, "0");

            //Permisos usuario Empresa
            Functions.scrooll(driver, LocalizadoresBO._011907_radConsulta);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011907_radConsulta, lista, "0", 0);

            //subRoles /permisos
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011919_consLlam, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011920_consConsPaq, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011921_consFin, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011922_consSerCont, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011923_facturacion, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011924_paquete, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011925_ayuda, lista, "0", 0);
            Functions.scrooll(driver, LocalizadoresBO._011926_administracion);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011926_administracion, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011928_Usuario, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011929_camPersonalizado, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011930_histMensaje, lista, "0", 0);

            //Asociación de numero de guatemala
            asociacionNumero(driver, lista, DatosBO._013107_númeroGTAgregar, "502");
            asociacionNumero(driver, lista, DatosBO._013109_numeroSVAgregar, "503");
            asociacionNumero(driver, lista, DatosBO._013110_numeroHNAgregar, "504");
            asociacionNumero(driver, lista, DatosBO._013111_numeroNIAgregar, "505");
            asociacionNumero(driver, lista, DatosBO._013112_numeroCRAgregar, "506");
            asociacionNumero(driver, lista, DatosBO._013113_numeroPAAgregar, "507");

            Functions.scrooll(driver, LocalizadoresBO._011933_buttonCrearUsuario);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011933_buttonCrearUsuario, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011934_copiUser, lista, "0", 5);
            Functions.ExistWhitParameter(driver, LocalizadoresBO._011936_alertCopyUser, 10);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011935_copiPassword, lista, "0", 10);
            Functions.ExistWhitParameter(driver, LocalizadoresBO._011937_alertCopyPassword, 10);
            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            driver.Quit();
        }
        //funcion para asociar número 
        public void asociacionNumero(ChromeDriver driver, List<String> lista, String numero, String CodPais)
        {
            Functions.SendKeysTab(driver, LocalizadoresBO._013104_ingresoCodigoPais, $"{CodPais}");
            Functions.clean(driver, "XPath", LocalizadoresBO._013103_número, lista, "0");
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._013103_número, $"{numero}", lista, "0");
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011932_asocServicio, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);

        }



    }
}
