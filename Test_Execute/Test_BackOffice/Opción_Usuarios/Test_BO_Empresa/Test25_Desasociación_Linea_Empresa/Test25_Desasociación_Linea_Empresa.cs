﻿
using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
#pragma warning disable CS1591
    public class Test25_Desasociación_Linea_Empresa
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
        public static bool Test1Called;
        static login login = new login();
        Desasociación_Linea_Empresa des = new Desasociación_Linea_Empresa();

        /*
        * CP01-28 Usuario (Empresa) Desasociación de linea
        */
        [Test]
        public void Test25_Desasociación_Linea_Empresa_()
        {
           
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                des.DesasociaciónLineaEmpresaBOClaroreado(driver, lista, LocalizadoresBO._013201_opciónBusqEmpresa, LocalizadoresBO._013903_textVadi, DatosBO._013805_adminSigP1, DatosBO._011902_usuarioAdminEmpresa, "Empresa");
           
        }
    }
}