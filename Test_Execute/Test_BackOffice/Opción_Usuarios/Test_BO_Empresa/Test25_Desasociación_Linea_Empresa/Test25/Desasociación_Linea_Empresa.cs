﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Threading;
using Test_Execute.Selenium.ResxBO;


namespace Test_Execute.Selenium.suit
{
    class Desasociación_Linea_Empresa
    {

        Selenium Functions = new Selenium();
       
        /*
      * Fecha: 27.02.22
      * Creador: Leopoldo Mendez Perez (QA)
      * Objetivo: Metodo para la Desociación de Lineas
      * Ultima actualización:23-02-2022
      * Responsable Ultima actualización:
      * Objetivo ultima actualización:
      */
        public void DesasociaciónLineaEmpresaBOClaroreado(ChromeDriver driver, List<string> lista, String xpath, String textoDeValidación, String siglas, String usuarioMultiPaís, String tipo)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 10);
            Functions.ClickButton(driver, "XPath", xpath, lista, "0", 10);
            Thread.Sleep(2000);
            Functions.ClickButton(driver, "XPath", xpath, lista, "0", 10);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010801_inputSearch, siglas + $"{usuarioMultiPaís}" + DatosBO._012002_SigP2, lista, "0");
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010802_columFecha, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010901_buttonUserEdit, lista, "0", 0);

            Functions.ClickButton(driver, "XPath", LocalizadoresBO._012601_editarServicioAsociado, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Thread.Sleep(2000);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._012501_desasociarLineaP1 + DatosBO._012301_servicioAsociar1 + LocalizadoresBO._012502_desasociarLineaP2, lista, "0", 0);
            Thread.Sleep(1000);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._012503_buttonDesasociar, lista, "0", 0);
            Functions.scrooll(driver, LocalizadoresBO._010903_buttonGuardar);
            Functions.ClickButton(driver, "Value", LocalizadoresBO._010903_buttonGuardar, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ExistWhitParameter(driver, textoDeValidación, 3);
            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            driver.Quit();

        }
    }
}
