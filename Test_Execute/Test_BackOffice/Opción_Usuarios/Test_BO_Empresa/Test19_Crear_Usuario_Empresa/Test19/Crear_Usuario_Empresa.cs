﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Threading;
using Test_Execute.Selenium.ResxBO;


namespace Test_Execute.Selenium.suit
{
    class Crear_Usuario_Empresa
    {

        Selenium Functions = new Selenium();
       
            /*
      * Fecha: 26.12.21
      * Creador: Leopoldo Mendez Perez (QA)
      * Objetivo: Alta de usuario Empresa 
      * Ultima actualización:23/02/2022
      * Responsable Ultima actualización:
      * Objetivo ultima actualización:
      */
        public void altaUsuarioEmpresaBOClaro(ChromeDriver driver, List<String> lista)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 10);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.scrooll(driver, LocalizadoresBO._010404_buttonCrearUsuario);
            Functions.ClickButton(driver, "Id", LocalizadoresBO._010402_buttonList, lista, "0", 10);

            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011901_linkEmpresa, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011914_tipo, lista, "0", 10);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011915_empresa, lista, "0", 0);

            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._011902_nombrEmpresa, DatosBO._011901_nombreEmpresa, lista, "0");
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._011908_usuarioAdmin, DatosBO._011902_usuarioAdminEmpresa, lista, "0");

            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._011903_correoAdminEmpresa, $"{DatosBO._011903_correoEmpresa}", lista, "0");
            Functions.scrooll(driver, LocalizadoresBO._011904_telAdminEmpresa);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._011904_telAdminEmpresa, $"{DatosBO._011904_telAdminEmpresa}", lista, "0");

            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011905_asignEjecPrev, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011916_selectEjecPrev, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011906_asignEjecPos, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011917_selectEjecPos, lista, "0", 0);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._011918_contasenia, $"{DatosBO._011905_contAdminEmpresa}", lista, "0");

            //Permisos usuario Empresa
            Functions.scrooll(driver, LocalizadoresBO._011907_radConsulta);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011907_radConsulta, lista, "0", 0);

            //subRoles /permisos
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011919_consLlam, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011920_consConsPaq, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011921_consFin, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011922_consSerCont, lista, "0", 0);

            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011923_facturacion, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011924_paquete, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011925_ayuda, lista, "0", 0);

            Functions.scrooll(driver, LocalizadoresBO._011926_administracion);

            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011926_administracion, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011928_Usuario, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011929_camPersonalizado, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011930_histMensaje, lista, "0", 0);


            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._011931_selectServicioInput, $"{DatosBO._011906_telAsociar}", lista, "0");
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011932_asocServicio, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.scrooll(driver, LocalizadoresBO._011933_buttonCrearUsuario);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011933_buttonCrearUsuario, lista, "0", 3);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Thread.Sleep(3000);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011934_copiUser, lista, "0", 5);
            Functions.ExistWhitParameter(driver, LocalizadoresBO._011936_alertCopyUser, 10);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._011935_copiPassword, lista, "0", 10);
            Functions.ExistWhitParameter(driver, LocalizadoresBO._011937_alertCopyPassword, 10);
            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            driver.Quit();
        }




    }
}
