﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
#pragma warning disable CS1591
    public class Test24_Validación_Mensaje_Linea_Asociada_Usuario_Empresa
    {
        //////////////////////////////////////////////////SELENIUM///////////////
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
        Validación_Mensaje_Linea_Asociada_Empresa val = new Validación_Mensaje_Linea_Asociada_Empresa();
        static login login = new login();

        /*
       * CP01-27 Usuario (Empresa) Validación de Mensaje "Linea ya esta asociada"
       * */
        [Test]
#pragma warning disable CS1591
        public void Test24_Validación_Mensaje_Linea_Asociada_Usuario_Empresa_()
        {
          
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                val.ValidaciónLineaAsociadaEmpresaBOClaroCreado(driver, lista, LocalizadoresBO._013201_opciónBusqEmpresa, LocalizadoresBO._012302_textLineaAsociada, "Empresa", DatosBO._013805_adminSigP1, DatosBO._011902_usuarioAdminEmpresa);
          
        }
    }
}