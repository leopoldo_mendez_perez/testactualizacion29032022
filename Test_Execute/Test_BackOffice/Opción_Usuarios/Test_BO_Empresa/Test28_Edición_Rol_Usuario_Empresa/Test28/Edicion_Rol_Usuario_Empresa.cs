﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Test_Execute.Selenium.ResxBO;
using Test_Execute.Selenium.ResxConfGlob;
using OpenQA.Selenium.Support.UI;
using System.Reflection;
using System.Configuration;
using AventStack.ExtentReports;
using NUnit.Framework;

namespace Test_Execute.Selenium.suit
{
    class Edicion_Rol_Usuario_Empresa
    {

        Selenium Functions = new Selenium();
        string ruta = @"C:\Logs_TE_TER\";
       
        string Orden;
        static String[] part; // variable para almacenar las cadenas desintegradas


        /*
            * Fecha:21.02.22
            * Creador: Leopoldo Méndez Pérez (QA)
            * Objetivo: Método para la edición de usuario Administrador
            * Ultima actualización:
            * Responsable Ultima actualización:
            * Objetivo ultima actualización:
            */
        public void EdicionRolUsuarioEmpresa(ChromeDriver driver, List<string> lista, String SeleccionarOpcionAdministrador ,String texto_Validación_de_Edicion,String xpath)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 10);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", xpath, lista, "0", 0);
            Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010801_inputSearch, DatosBO._012001_SigP1 + $"{ DatosBO._01261_cond1}" + DatosBO._012002_SigP2, lista, "0");
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010802_columFecha, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010901_buttonUserEdit, lista, "0", 0);
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._012801_Editar_Perfil, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            part =Functions.SeparaTexto(DatosBO._012802_Rol_Usuario_Empresa);
            String numerofin, textofin;
            Thread.Sleep(1000);
            for (int i=0;i<part.Length;i++) {
                //Lineas de codigo para extraer el estado del rol y tipo de Rol;
              
                textofin = (part[i].Substring(2)).Replace(" ", "");
                if (i == 0)
                {
                    numerofin = (part[i].Substring(0, 1)).Replace(" ", "");
                }
                else {
                    numerofin = (part[i].Substring(1, 1)).Replace(" ", "");
                }
                //---------------------------------------


                //ROL Consultas
                if (numerofin == "1" && textofin == "Consultas")
                {
                    Functions.ClickButton(driver, "XPath", LocalizadoresBO._011907_radConsulta, lista, "0", 10);
                    Console.WriteLine("Rol Editado: " + textofin);                }
                //ROL Consulta de detalles de llamadas
                if (numerofin == "1" && textofin == "Consultasdedetallesdellamadas")
                {
                    Functions.ClickButton(driver, "XPath", LocalizadoresBO._011919_consLlam, lista, "0", 0);
                    Console.WriteLine("Rol Editado: " + textofin);
                }
                //ROL Consulta de consumos de paquetes
                if (numerofin == "1" && textofin == "Consultasdeconsumosdepaquetes")
                {
                    Functions.ClickButton(driver, "XPath", LocalizadoresBO._011920_consConsPaq, lista, "0", 0);
                    Console.WriteLine("Rol Editado: " + textofin);
                }
                //ROL Consulta de financiamiento
                if (numerofin == "1" && textofin == "Consultasdefinanciamientos")
                {
                    
                    Functions.ClickButton(driver, "XPath", LocalizadoresBO._011921_consFin, lista, "0", 0);
                    Console.WriteLine("Rol Editado: " + textofin);
                }
                //ROL Consulta de servicios contratados
                if (numerofin == "1" && textofin == "Consultasdeservicioscontratados")
                {
                    Functions.ClickButton(driver, "XPath", LocalizadoresBO._011922_consSerCont, lista, "0", 0);
                    Console.WriteLine("Rol Editado: " + textofin);
                }

                //ROL Facturación
                if (numerofin == "1" && textofin == "Facturación")
                {
                    Thread.Sleep(1000);
                    Functions.ClickButton(driver, "XPath", LocalizadoresBO._011923_facturacion, lista, "0", 0);
                    Console.WriteLine("Rol Editado: " + textofin);

                }
                //ROL Paquetes
                if (numerofin == "1" && textofin == "Paquetes")
                {
                    Thread.Sleep(1000);
                    Functions.ClickButton(driver, "XPath", LocalizadoresBO._011924_paquete, lista, "0", 0);
                    Console.WriteLine("Rol Editado: " + textofin);

                }
                //ROL Ayuda
                if (numerofin == "1" && textofin == "Ayuda")
                {
                    Thread.Sleep(1000);
                    Functions.ClickButton(driver, "XPath", LocalizadoresBO._011925_ayuda, lista, "0", 0);
                    Console.WriteLine("Rol Editado: " + textofin);

                }
                //ROL Administración
                if (numerofin == "1" && textofin == "Administración")
                {
                    Thread.Sleep(1000);
                    Functions.ClickButton(driver, "XPath", LocalizadoresBO._011926_administracion, lista, "0", 0);
                    Console.WriteLine("Rol Editado: " + textofin);
                    Functions.scrooll(driver, LocalizadoresBO._011928_Usuario);

                }
                //ROL Usuarios
                if (numerofin == "1" && textofin == "Usuarios")
                {
                    Thread.Sleep(1000);
                    Functions.ClickButton(driver, "XPath", LocalizadoresBO._011928_Usuario, lista, "0", 0);
                    Console.WriteLine("Rol Editado: " + textofin);

                }
                //ROL Campos Personalizados
                if (numerofin == "1" && textofin == "Campospersonalizados")
                {
                    Thread.Sleep(1000);
                    Functions.ClickButton(driver, "XPath", LocalizadoresBO._011929_camPersonalizado, lista, "0", 0);
                    Console.WriteLine("Rol Editado: " + textofin);

                }
                //ROL Historia de mensajes
                if (numerofin == "1" && textofin == "Historialdemensajes")
                {
                    Thread.Sleep(1000);
                    Functions.ClickButton(driver, "XPath", LocalizadoresBO._011930_histMensaje, lista, "0", 0);
                    Console.WriteLine("Rol Editado: " + textofin);

                }
            }
            Functions.ClickButton(driver, "Value", LocalizadoresBO._010903_buttonGuardar, lista, "0", 0);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.scrooll(driver, texto_Validación_de_Edicion);
            Functions.ExistWhitParameter(driver, texto_Validación_de_Edicion, 3);
            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            driver.Quit();
        }
    }
}
