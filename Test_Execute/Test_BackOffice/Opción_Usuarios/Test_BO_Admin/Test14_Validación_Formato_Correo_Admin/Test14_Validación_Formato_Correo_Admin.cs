﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test14_Validation_email_format_Admi
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
      
        static login login = new login();
        ValidarFormatoCorreoAdministrador correo = new ValidarFormatoCorreoAdministrador();
        /*
        * CP01-14	Crear usuario (Administrador) Validación de formato de correo
        */
        [Test]
        public void Test14_Validation_email_format_Admi_()
        {
            
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                correo.altaUsuarioBOClaroValidacionFormatoCorreo(driver, lista, LocalizadoresBO._011101_linkAdministradores);
           
        }
    }
}