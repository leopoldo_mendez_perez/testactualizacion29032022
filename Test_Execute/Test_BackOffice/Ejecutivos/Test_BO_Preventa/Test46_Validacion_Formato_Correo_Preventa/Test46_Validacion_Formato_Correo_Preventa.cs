﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;


namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test46_Validacion_Formato_Correo_Preventa
    {
        static ChromeDriver driver;
        static Config conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
        static login login = new login();
        Validacion_Formato_Correo_Preventa c = new Validacion_Formato_Correo_Preventa();

        /*
        CP01-46	Crear usuario(Administrador) Validacion de campos vacios (user)
        */
        [Test]
        #pragma warning disable CS1591
        public void Test46_Validacion_Formato_Correo_EjecutivoPreventa()
        {
            
                driver = conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                c.ValidacionFormatoCorreo_Preventa(driver, lista, LocalizadoresBO._014301_opcEjecutivos);
         
        }
    }
}
