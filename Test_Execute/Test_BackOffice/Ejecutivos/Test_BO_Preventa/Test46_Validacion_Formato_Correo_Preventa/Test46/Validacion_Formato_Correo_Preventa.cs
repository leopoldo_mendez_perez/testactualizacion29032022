﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Threading;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace Test_Execute.Selenium.suit
{
    class Validacion_Formato_Correo_Preventa
    {
        Selenium Functions = new Selenium();
        /*
        * Fecha: 25.03.2022
        * Creador: Orlanddo Alberto López Brenes (QA) 
        * Objetivo: Método para validación del formato correo en el formulario de alta de usuario ejecutivo prevenrta en el portal BO
        * Ultima actualización:
        * Responsable Última actualización:
        * Objetivo última actualización:
        */

        public void ValidacionFormatoCorreo_Preventa(ChromeDriver driver, List<String> lista, String XPath)
        {
            try
            {
                Functions.ClickButton(driver, "XPath", LocalizadoresBO._014301_opcEjecutivos, lista, "0", 10);
                Functions.cargando(driver, LocalizadoresBO._011005_procesando);
                Functions.ClickButton(driver, "XPath", LocalizadoresBO._014302_btnCrearEjecutivoPreventa, lista, "0", 10);
                Functions.cargando(driver, LocalizadoresBO._011005_procesando);

                //Selección de opción PREVENTA
                Functions.ClickButton(driver, "XPath", LocalizadoresBO._014303_tipoEjecutivo, lista, "0", 10);
                Functions.ClickButton(driver, "XPath", LocalizadoresBO._014304_opcEjecutivoPreventa, lista, "0", 10);

                Functions.SetTextVal(driver, "XPath", LocalizadoresBO._014305_campoNombre, $"{DatosBO._014301_nombreEjecutivoPreventa}", lista, "0");
                var jsonFolder = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @".\\");
                //SP significa Clase super Padre
                var pathFinal = jsonFolder.Replace("bin\\Debug\\.\\", "") + "Imag\\ejecutivo.png";
                Functions.SetTextVal(driver, "XPath", LocalizadoresBO._014311_inputFile, pathFinal, lista, "0");
                Functions.scrooll(driver, LocalizadoresBO._014302_btnCrearEjecutivoPreventa);

                Functions.SetTextVal(driver, "XPath", LocalizadoresBO._014307_campoCorreo, $"{DatosBO._014601_errorFormatoCorreo}", lista, "0");
                Functions.SetTextVal(driver, "XPath", LocalizadoresBO._014308_campoTelefono, $"{DatosBO._014303_Telefono}", lista, "0");

                Functions.ClickButton(driver, "XPath", LocalizadoresBO._014309_btnCrearEjecutivoPreventa, lista, "0", 10);
                Functions.cargando(driver, LocalizadoresBO._011005_procesando);
                Functions.Equals(driver, LocalizadoresBO._014601_msjValidacionFormatoCorreo, DatosBO._014602_errorText_FormatoCorreo);


            }
            catch (Exception e)
            {
                driver.Quit();
                Assert.Fail("Error " + e);
            }
            Thread.Sleep(5000);
            driver.Quit();
        }
    }
}
