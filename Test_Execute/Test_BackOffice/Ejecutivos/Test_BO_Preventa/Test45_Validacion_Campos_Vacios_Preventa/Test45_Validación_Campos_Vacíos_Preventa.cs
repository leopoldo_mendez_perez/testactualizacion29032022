﻿using OpenQA.Selenium.Chrome;
using Test_Execute.Selenium;
using Test_Execute.Selenium.suit;
using System.Collections.Generic;
using System;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    #pragma warning disable CS1591
    public class Test45_Validación_Campos_Vacios_Preventa
    {

        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        private static List<String> lista = new List<String>();
        static login login = new login();
        ValidacionCamposVacíos_Preventa v = new ValidacionCamposVacíos_Preventa();
        /*
        CP01-45	Crear ejecutivo PREVENTA_Validacion de campos vacíos
        */
        [Test]
        public void Test45_Validacion_Campos_Vacios_Preventa()
        {
                driver = Conf.ConfigChrome();
                login.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                v.ejecutivoPreventaValidacionCamposVacios(driver, lista, LocalizadoresBO._014302_btnCrearEjecutivoPreventa);

            
        }
    }
}
