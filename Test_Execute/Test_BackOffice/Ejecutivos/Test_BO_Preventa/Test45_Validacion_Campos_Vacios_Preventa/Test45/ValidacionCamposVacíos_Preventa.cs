﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Threading;
using Test_Execute.Selenium.ResxBO;
using NUnit.Framework;

namespace Test_Execute.Selenium.suit
{
    class ValidacionCamposVacíos_Preventa
    {

        Selenium Functions = new Selenium();

        /*
         * Fecha: 25.03.22
         * Creador: Orlando López Brenes (QA)
         * Objetivo: Método validación de campos vacíos al tratar de crear usuario ejecutivo preventa
         * Ultima actualización:
         * Responsable Ultima actualización:
         * Objetivo ultima actualización:
         */

        public void ejecutivoPreventaValidacionCamposVacios(ChromeDriver driver, List<String> lista, String xpath)
        {
            try
            {
                Functions.ClickButton(driver, "XPath", LocalizadoresBO._014301_opcEjecutivos, lista, "0", 10);
                Functions.cargando(driver, LocalizadoresBO._011005_procesando);
                Thread.Sleep(2000);
                Functions.ClickButton(driver, "XPath", LocalizadoresBO._014302_btnCrearEjecutivoPreventa, lista, "0", 10);
                Functions.cargando(driver, LocalizadoresBO._011005_procesando);
                Functions.scrooll(driver, LocalizadoresBO._014309_btnCrearEjecutivoPreventa);
                Functions.ClickButton(driver, "XPath", LocalizadoresBO._014309_btnCrearEjecutivoPreventa, lista, "0", 10);
                Functions.cargando(driver, LocalizadoresBO._011005_procesando);
                Functions.Equals(driver, LocalizadoresBO._014501_msjAdvertCampoTipoEjecutivo, DatosBO._014501_errorText_TipoEjecutivo);
                Functions.Equals(driver, LocalizadoresBO._014502_msjAdvertCampoNombre, DatosBO._014502_errorText_Nombre);
                Functions.Equals(driver, LocalizadoresBO._014503_msjAdvertCampoCorreoElectrónico, DatosBO._014503_errorText_Correo);
                Functions.Equals(driver, LocalizadoresBO._014504_msjAdvertCampoTeléfono, DatosBO._014504_errorText_Telefono);
                
            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            Thread.Sleep(10000);
            driver.Quit();

        }
    }
}
