﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Test_Execute.Selenium.ResxBO;
using Test_Execute.Selenium.ResxConfGlob;
using OpenQA.Selenium.Support.UI;
using System.Reflection;
using System.Configuration;
using AventStack.ExtentReports;
using NUnit.Framework;

namespace Test_Execute.Selenium.suit
{
     class Busqueda_Ejecutivo_Creado_Preventa
    {
        Selenium Functions = new Selenium();

        /*
        * Fecha: 24.03.22
        * Creador: Leopoldo Mendez Perez (QA)
        * Objetivo: Método para de busqueda de usuario Administrador Creado
        * Ultima actualización:
        * Responsable Ultima actualización:
        * Objetivo ultima actualización:
        */

        public void BusquedaEjecutivoCreadoPreventa(ChromeDriver driver, List<String> lista, String xpath)
        {
            try
            {
                Functions.ClickButton(driver, "XPath", LocalizadoresBO._014301_opcEjecutivos, lista, "0", 10);
                Functions.cargando(driver, LocalizadoresBO._011005_procesando);
                Functions.ClickButton(driver, "XPath", xpath, lista, "0", 0);
                Functions.SetTextVal(driver, "XPath", LocalizadoresBO._010801_inputSearch, $"{DatosBO._014301_nombreEjecutivoPreventa}", lista, "0");
                Functions.ExistWhitParameter(driver, LocalizadoresBO._014402_input_Búsqueda, 10);
            }
            catch (Exception ex)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error" + ex);
            }
            Thread.Sleep(10000);
            driver.Quit();
        }
    }
}
