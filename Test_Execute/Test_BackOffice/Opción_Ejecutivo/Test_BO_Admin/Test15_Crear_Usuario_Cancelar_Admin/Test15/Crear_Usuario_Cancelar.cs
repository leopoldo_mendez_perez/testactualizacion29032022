﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using Test_Execute.Selenium.ResxBO;

namespace Test_Execute.Selenium.suit
{
    class CancelAdministrador
    {
        Selenium Functions = new Selenium();
        /*
      * Fecha: 03.02.22
      * Creador: Ruben Velazquez Patiño (bSide)
      * Modificado: Leopoldo Mendez Perez (QA) Fecha: 22.02.22
      * Objetivo: Método para validación el botón Cancelar en el formulario de alta de usuario para BO para clientes
      * Ultima actualización:
      * Responsable última actualización:
      * Objetivo ultima actualización:
      */
        public void altaUsuarioBOClaroCrearUsuarioBtnCancelar(ChromeDriver driver, List<String> lista, String xpath)
        {
            try { 
            Functions.ClickButton(driver, "XPath", LocalizadoresBO._010401_linkUsuarioHome, lista, "0", 10);
            Functions.scrooll(driver, LocalizadoresBO._010404_buttonCrearUsuario);
            Functions.ClickButton(driver, "Id", LocalizadoresBO._010402_buttonList, lista, "0", 5);
            Functions.ClickButton(driver, "XPath", xpath, lista, "0", 0);
            Functions.scrooll(driver, LocalizadoresBO._010701_buttonCancelar);
            Functions.ClickButton(driver, "Value", LocalizadoresBO._010701_buttonCancelar, lista, "0", 0);
            Functions.ExistWhitParameter(driver, LocalizadoresBO._010702_ValidCancelar, 3);
            }
            catch (Exception e)
            {
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
            }
            driver.Quit();
        }
    }
}
