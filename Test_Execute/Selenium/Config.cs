﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;

namespace Test_Execute.Selenium
{
    class Config
    {
        public ChromeDriver ConfigChrome()
        {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.BinaryLocation = "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe";

            ChromeDriver driver = new ChromeDriver(chromeOptions);
       
            return driver;
        }

        public void ConfigIE()
        {
            InternetExplorerOptions options = new InternetExplorerOptions();
            ChromeDriver driver = new ChromeDriver();
        }
        public ChromeDriver ConfigChrome2()
        {
            ChromeOptions options2 = new ChromeOptions();
            options2.AddArgument("start-maximized");
            ChromeDriver driver2 = new ChromeDriver(options2);
            return driver2;
        }
    }
}
